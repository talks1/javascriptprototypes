import Debug from 'debug'
let debug = Debug('app')

export const OperationV1 = () => {
    let goal='Factory method which creates one instance of many'
    let drawback='While this works Each instance has a copy of all state and methods'
    let state='private state'
    let operation = {
        method: ()=>{
            return state
        }
    }
    return operation
}

const OperationV2Methods = {
    method(){
        return this.state
    }
    //note method: ()=>this.state doesnt work because anonymous dont have this
}
export const OperationV2 = () => {
    let goal='Factory method which creates one instance of many'
    let drawback='This is a single copy of the Methods but these are separated into a separate object and the Operation needs to reference them'
    let methods = OperationV2Methods
    let operation = {}
    operation.state = 'private state'
    operation.method = methods.method
    return operation
}


const OperationV3Methods = {
    method(){
        return this.state
    }
    //note method: ()=>this.state doesnt work because anonymous dont have this
}
export const OperationV3 = () => {
    let goal='Factory method which creates one instance of many'
    let drawback='The Object.create sets the methods as a __proto__ on the object so there is only a single copy'
    let methods = OperationV3Methods
    let operation = Object.create(OperationV3Methods)
    operation.state = 'private state'
    return operation
}


export const OperationV4 = function(){
    let goal='Factory method which creates one instance of many'
    let drawback='Here the set the prototype of the function to be its prototype'
    let operation = Object.create(OperationV4.prototype)
    operation.state = 'private state'
    return operation
}
OperationV4.prototype.method = function() {
    return this.state
} 


export const OperationV5 = function(){
    let goal='Factory method which creates one instance of many'
    let drawback='While this works Each instance has a copy of all state and methods'
    //let operation = Object.create(OperationV4.prototype)
    this.state = 'private state'
    //return operation
}
OperationV5.prototype.method = function() {
    return this.state
}


// -----------Notes--------
// classes are just Function created as above
// arrow functions dont have this ... not intended to be 'organized'


// Question : Transition towards use of Prototypes.