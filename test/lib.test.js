import { describe } from 'riteway'

import { OperationV1,OperationV2, OperationV3, OperationV4, OperationV5 } from '../index'

describe('Operation Version 1', async (assert) => {
  const operation = OperationV1()
  let result = operation.method()

  console.log(result)
      assert({
        given: 'invoking method on a Version 1 Operation',
        should: 'give us the private state',
        actual: result,
        expected: 'private state'
      })
    }
)


describe('Operation Version 2', async (assert) => {
  const operation = OperationV2()
  let result = operation.method()
      assert({
        given: 'invoking method on a Version 2 Operation',
        should: 'give us the private state',
        actual: result,
        expected: 'private state'
      })
    }
)


describe('Operation Version 3', async (assert) => {
  const operation = OperationV3()
  //console.log(operation.__proto__)
  let result = operation.method()
      assert({
        given: 'invoking method on a Version 3 Operation',
        should: 'give us the private state',
        actual: result,
        expected: 'private state'
      })
    }
)


describe('Operation Version 4', async (assert) => {
  const operation = OperationV4()
  //console.log(operation.__proto__)
  let result = operation.method()
      assert({
        given: 'invoking method on a Version 3 Operation',
        should: 'give us the private state',
        actual: result,
        expected: 'private state'
      })
    }
)


describe('Operation Version 5', async (assert) => {
  const operation = new OperationV5()
  console.log(operation.__proto__)
  let result = operation.method()
      assert({
        given: 'invoking method on a Version 3 Operation',
        should: 'give us the private state',
        actual: result,
        expected: 'private state'
      })
    }
)

describe.only('This', async (assert) => {

  let foo = ()=>{
    console.log(this)
  }
  foo()


})