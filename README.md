# Prototypes

I cannot say that I know how to use Javascript prototypes.

I have tried multiple times and never felt I could understand it well enough to feel it was going to simplify my code.

[This tutorial video](https://tylermcginnis.com/beginners-guide-to-javascript-prototype/)  inspired me to take another stab at using prototypes.


## How to use

See the main.js file which has several different versions of a factory function.

I had been using the first version and wanted to evolve pass its limitations.  The video explained how I could do this.

### To Develop

Develop the component by running the server
```
npm run start
```

### To Test

This test the components by rendering them can checking generated dom.
```
npm run test
```

